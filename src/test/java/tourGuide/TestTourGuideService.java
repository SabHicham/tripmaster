package tourGuide;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import org.junit.Before;
import org.junit.Test;
import rewardCentral.RewardCentral;
import tourGuide.helper.InternalTestHelper;
import tourGuide.repository.TourGuideRepository;
import tourGuide.service.GpsUtilService;
import tourGuide.service.RewardsService;
import tourGuide.service.TourGuideService;
import tourGuide.service.TripPriceService;
import tourGuide.user.User;
import tripPricer.Provider;
import tripPricer.TripPricer;


public class TestTourGuideService {
	private GpsUtilService   gpsUtilsService;
	private RewardsService rewardsService;
	private TripPriceService tripPricerService;

	private TourGuideRepository tourGuideRepository;

	@Before
	public void setUp() {
		Locale.setDefault(Locale.US);

		this.gpsUtilsService   = new GpsUtilService(new GpsUtil());
		this.rewardsService    = new RewardsService(new RewardCentral());
		this.tripPricerService = new TripPriceService(new TripPricer());
		this.tourGuideRepository =new TourGuideRepository();
	}

	// done: probleme gpsUtil(l 29)-> Locale.setDefault(Locale.US)
	@Test
	public void getUserLocation() throws ExecutionException, InterruptedException {
		GpsUtil gpsUtil = new GpsUtil();
		GpsUtilService gpsUtilService = new GpsUtilService(gpsUtil);
		RewardsService rewardsService = new RewardsService(new RewardCentral());
		rewardsService.setGpsUtilService(gpsUtilService);
		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(new GpsUtilService(gpsUtil), rewardsService, new TripPriceService(new TripPricer()), new TourGuideRepository());
		
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		VisitedLocation visitedLocation = tourGuideService.trackUserLocation(user).get();
		tourGuideService.tracker.stopTracking();
		assertTrue(visitedLocation.userId.equals(user.getUserId()));
	}
	// DONE: ajout du repository
	@Test
	public void addUser() {

		GpsUtil gpsUtil = new GpsUtil();
		RewardsService rewardsService = new RewardsService( new RewardCentral());
		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(new GpsUtilService(gpsUtil), rewardsService, new TripPriceService(new TripPricer()), new TourGuideRepository() );
		tourGuideService.setTourGuideRepository(tourGuideRepository);

		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		User user2 = new User(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

		tourGuideService.addUser(user);
		tourGuideService.addUser(user2);
		
		User retrivedUser = tourGuideService.getUser(user.getUserName());
		User retrivedUser2 = tourGuideService.getUser(user2.getUserName());

		tourGuideService.tracker.stopTracking();
		
		assertEquals(user, retrivedUser);
		assertEquals(user2, retrivedUser2);
	}

	// DONE: ajout du repository
	@Test
	public void getAllUsers() {
		GpsUtil gpsUtil = new GpsUtil();
		RewardsService rewardsService = new RewardsService(new RewardCentral());
		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(new GpsUtilService(gpsUtil), rewardsService, new TripPriceService(new TripPricer()), new TourGuideRepository());
		tourGuideService.setTourGuideRepository(tourGuideRepository);

		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		User user2 = new User(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

		tourGuideService.addUser(user);
		tourGuideService.addUser(user2);
		
		List<User> allUsers = tourGuideService.getAllUsers();

		tourGuideService.tracker.stopTracking();
		
		assertTrue(allUsers.contains(user));
		assertTrue(allUsers.contains(user2));
	}
	// done: probleme gpsUtil(l 29)-> Locale.setDefault(Locale.US)
	@Test
	public void trackUser() throws ExecutionException, InterruptedException {
		GpsUtil gpsUtil = new GpsUtil();
		GpsUtilService gpsUtilService = new GpsUtilService(gpsUtil);
		RewardsService rewardsService = new RewardsService( new RewardCentral());
		rewardsService.setGpsUtilService(gpsUtilService);

		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(new GpsUtilService(gpsUtil), rewardsService, new TripPriceService(new TripPricer()), new TourGuideRepository());
		
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		VisitedLocation visitedLocation = tourGuideService.trackUserLocation(user).get();
		
		tourGuideService.tracker.stopTracking();
		
		assertEquals(user.getUserId(), visitedLocation.userId);
	}

	// done: probleme gpsUtil(l 29)-> Locale.setDefault(Locale.US)
	/*@Test
	public void getNearbyAttractions() throws ExecutionException, InterruptedException {
		GpsUtil gpsUtil = new GpsUtil();
		GpsUtilService gpsUtilService = new GpsUtilService(gpsUtil);
		RewardsService rewardsService = new RewardsService( new RewardCentral());
		rewardsService.setGpsUtilService(gpsUtilService);
		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(new GpsUtilService(gpsUtil), rewardsService, new TripPriceService(new TripPricer()), new TourGuideRepository());
		
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		VisitedLocation visitedLocation = tourGuideService.trackUserLocation(user).get();
		
		List<Attraction> attractions = tourGuideService.getNearByAttractions(visitedLocation).get();
		
		tourGuideService.tracker.stopTracking();
		
		assertEquals(0, attractions.size());
	}*/

	@Test
	public void getNearbyAttractions() throws ExecutionException, InterruptedException {
		GpsUtil gpsUtil = new GpsUtil();
		GpsUtilService gpsUtilService = new GpsUtilService(gpsUtil);
		RewardsService rewardsService = new RewardsService( new RewardCentral());
		rewardsService.setGpsUtilService(gpsUtilService);
		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(new GpsUtilService(gpsUtil), rewardsService, new TripPriceService(new TripPricer()), new TourGuideRepository());

		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		VisitedLocation visitedLocation = tourGuideService.trackUserLocation(user).get();

		List<Attraction> attractions = tourGuideService.getNearByAttractions(visitedLocation).get();

		tourGuideService.tracker.stopTracking();

		assertEquals(0, attractions.size());
	}
	// DONE: IMPLEMENT METHODE, le probléme vient probablement de tripPricer
	@Test
	public void getTripDeals() {
		GpsUtil gpsUtil = new GpsUtil();
		RewardsService rewardsService = new RewardsService( new RewardCentral());
		InternalTestHelper.setInternalUserNumber(0);
		TourGuideService tourGuideService = new TourGuideService(new GpsUtilService(gpsUtil), rewardsService, new TripPriceService(new TripPricer()), new TourGuideRepository());
		tourGuideService.setTourGuideRepository(tourGuideRepository);
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");

		List<Provider> providers = tourGuideService.getTripDeals(user);
		
		tourGuideService.tracker.stopTracking();
		
		assertEquals(5, providers.size());
	}
	
	
}
