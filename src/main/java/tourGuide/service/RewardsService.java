package tourGuide.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import rewardCentral.RewardCentral;
import tourGuide.user.User;
import tourGuide.user.UserReward;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

//@Service
public class RewardsService {

    private static final double STATUTE_MILES_PER_NAUTICAL_MILE = 1.15077945;

	// proximity in miles
    private int defaultProximityBuffer = 10;
	private int proximityBuffer = defaultProximityBuffer;
	private int attractionProximityRange = 200;
	@Autowired
	private GpsUtilService gpsUtilService;
	@Autowired
	private RewardCentral rewardsCentral;

	private final ExecutorService executorService = Executors.newFixedThreadPool(1000);

	public RewardsService() {

	}

	public RewardsService(RewardCentral rewardsCentral) {
		this.rewardsCentral = rewardsCentral;

	}

	public RewardsService(RewardCentral rewardsCentral, GpsUtilService gpsUtilService) {
		this.gpsUtilService= gpsUtilService;
		this.rewardsCentral=rewardsCentral;
	}

	public void setProximityBuffer(int proximityBuffer) {
		this.proximityBuffer = proximityBuffer;
	}
	
	public void setDefaultProximityBuffer() {
		proximityBuffer = defaultProximityBuffer;
	}
	


	public boolean isWithinAttractionProximity(Attraction attraction, Location location) {
		return gpsUtilService.getDistance(attraction, location) > attractionProximityRange ?
				false : true;
	}
	
	public boolean nearAttraction(VisitedLocation visitedLocation, Attraction attraction) {
		//return gpsUtilService.getDistance(attraction, visitedLocation.location) > proximityBuffer ? false : true;.
		double distance = gpsUtilService.getDistance(attraction, visitedLocation.location);
		return distance < this.proximityBuffer;
	}

	public int getRewardPoints(Attraction attraction, User user) {
		return rewardsCentral.getAttractionRewardPoints(attraction.attractionId, user.getUserId());
	}


	public void setGpsUtilService(GpsUtilService gpsUtilService) {
		this.gpsUtilService = gpsUtilService;
	}

	public void calculateRewards(User user) throws ExecutionException, InterruptedException {

		this.executorService.execute(() -> {

			Iterable<VisitedLocation> userLocations = new ArrayList<>(user.getVisitedLocations());
			List<Attraction> attractions   = this.gpsUtilService.getAllAttraction();

			for (VisitedLocation visitedLocation : userLocations) {

				for (Attraction attraction : attractions) {

					boolean noMatch = user.getUserRewards()
							.stream()
							.filter(r -> r.attraction.attractionName.equals(attraction.attractionName)).collect(Collectors.toList()).size() == 0;

					if (noMatch) {
						boolean isNear = nearAttraction(visitedLocation, attraction);
						if(isNear) {
							UserReward userReward = new UserReward(visitedLocation, attraction, getRewardPoints(attraction, user));
							user.addUserReward(userReward);
						}
					}
				}
			}

		});


	}
}
