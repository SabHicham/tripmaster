package tourGuide.service;

import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tripPricer.Provider;
import tripPricer.TripPricer;

import java.util.List;
import java.util.UUID;

@Service
public class TripPriceService {
    private final TripPricer tripPricer;

    @Value("${tripPricer.api.key:test-server-api-key}")
    private       String     testServerApiKey;
    public TripPriceService(TripPricer tripPricer) {

        this.tripPricer = tripPricer;

    }



    /*public List<Provider> getPrice(String tripPricerApiKey, UUID userId, int numberOfAdults, int numberOfChildren, int tripDuration, int cumulatativeRewardPoints) {
        List<Provider> prices = tripPricer.getPrice(tripPricerApiKey, userId,numberOfAdults, numberOfChildren, tripDuration,cumulatativeRewardPoints);

        return prices;
    }*/
    public List<Provider> getPrice(UUID attractionId, int adults, int children, int nightsStay, int rewardsPoints) {

        return this.tripPricer.getPrice(
                this.testServerApiKey, attractionId, adults, children, nightsStay, rewardsPoints);
    }

}
