package tourGuide.service;

import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import gpsUtil.location.Location;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;

import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import tourGuide.dto.NearAttractionDto;
import tourGuide.helper.InternalTestHelper;
import tourGuide.repository.TourGuideRepository;
import tourGuide.tracker.Tracker;
import tourGuide.user.User;
import tourGuide.user.UserPreferences;
import tourGuide.user.UserReward;
import tripPricer.Provider;

@Service
public class TourGuideService {

	private Logger logger = LoggerFactory.getLogger(TourGuideService.class);

	private GpsUtilService gpsUtilService;

	private  RewardsService rewardsService;

	private  TripPriceService tripPriceService;

	public  Tracker tracker;

	private TourGuideRepository tourGuideRepository;
	private final ExecutorService executorService = Executors.newFixedThreadPool(1000);

	public TourGuideService() {

	}

	public TourGuideService(GpsUtilService gpsUtilService, RewardsService rewardsService) {
		this.gpsUtilService=gpsUtilService;
		this.rewardsService=rewardsService;
	}


	public void setTourGuideRepository(TourGuideRepository tourGuideRepository) {
		this.tourGuideRepository = tourGuideRepository;
	}

	boolean testMode = true;

	public TourGuideService(GpsUtilService gpsUtilService, RewardsService rewardsService, TripPriceService tripPriceService, TourGuideRepository tourGuideRepository) {
		this.gpsUtilService = gpsUtilService;
		this.rewardsService = rewardsService;
		this.tripPriceService = tripPriceService;
		this.tourGuideRepository = tourGuideRepository;

		if(testMode) {
			logger.info("TestMode enabled");
			logger.debug("Initializing users");
			initializeInternalUsers();
			logger.debug("Finished initializing users");
		}
		tracker = new Tracker(this);
		addShutDownHook();
	}


	public List<UserReward> getUserRewards(User user) {
		return user.getUserRewards();
	}

	private void initializeInternalUsers() {

		IntStream.range(0, InternalTestHelper.getInternalUserNumber()).forEach(i -> {
			String userName = "internalUser" + i;
			String phone = "000";
			String email = userName + "@tourGuide.com";
			User user = new User(UUID.randomUUID(), userName, phone, email);
			this.gpsUtilService.generateUserLocationHistory(user);

			tourGuideRepository.addUser(user);
		});
		logger.debug("Created " + InternalTestHelper.getInternalUserNumber() + " internal test users.");
	}
	
	public User getUser(String userName) {
		return tourGuideRepository.getUser(userName);
	}
	
	public List<User> getAllUsers() {
		return tourGuideRepository.getAllUsers();
	}

	public CompletableFuture<VisitedLocation> getUserLocation(User user) {

		return user.getVisitedLocations().isEmpty() ? this.trackUserLocation(user) : CompletableFuture.completedFuture(
				user.getLastVisitedLocation());
	}

	// TODO : probleme touGuideService (ligne 83)
	public void addUser(User user) {
		tourGuideRepository.addUser(user);
	}
	
	public List<Provider> getTripDeals(User user) {
		int cumulatativeRewardPoints = user.getUserRewards().stream().mapToInt(i -> i.getRewardPoints()).sum();
		List<Provider> providers = tripPriceService.getPrice(user.getUserId(), user.getUserPreferences().getNumberOfAdults(),
				user.getUserPreferences().getNumberOfChildren(), user.getUserPreferences().getTripDuration(), cumulatativeRewardPoints);
		user.setTripDeals(providers);
		return providers;
	}
	
	public CompletableFuture<VisitedLocation> trackUserLocation(User user) {
		return CompletableFuture.supplyAsync(() -> {
			VisitedLocation visitedLocation = this.gpsUtilService.getUserLocation(user.getUserId());
			user.addToVisitedLocations(visitedLocation);

			return visitedLocation;
		}, this.executorService).thenApplyAsync((visitedLocation) -> {
			try {
				this.calculateRewards(user);
			} catch (ExecutionException e) {
				throw new RuntimeException(e);
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
			return visitedLocation;
		}, this.executorService);
	}

	public CompletableFuture<List<Attraction>> getNearByAttractions(VisitedLocation visitedLocation) {
		CompletableFuture<List<Attraction>> future
				= CompletableFuture.supplyAsync(() -> {List<Attraction> nearbyAttractions = new ArrayList<>();
			for(Attraction attraction : gpsUtilService.getAllAttraction()) {
				if(rewardsService.isWithinAttractionProximity(attraction, visitedLocation.location)) {
					nearbyAttractions.add(attraction);
				}

			}

			return nearbyAttractions;
				}
		);

		return future;
	}
	
	private void addShutDownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread() { 
		      public void run() {
		        tracker.stopTracking();
		      } 
		    }); 
	}
	
	/**********************************************************************************
	 * 
	 * Methods Below: For Internal Testing
	 * 
	 **********************************************************************************/
	private static final String tripPricerApiKey = "test-server-api-key";
	// Database connection will be used for external users, but for testing purposes internal users are provided and stored in memory

	public void calculateRewards(User user) throws ExecutionException, InterruptedException {

		//this.executorService.execute(() -> {
			//List<VisitedLocation> userLocations = user.getVisitedLocations();
			//List<Attraction> attractions = gpsUtilService.getAllAttraction();
/*
		for(VisitedLocation visitedLocation : userLocations) {
			for(Attraction attraction : attractions) {
				int sameRewards = tourGuideService.getExecutorService().shutdown();
				if(user.getUserRewards().stream().filter(r -> r.attraction.attractionName.equals(attraction.attractionName)).count() == 0) {
					if(rewardsService.nearAttraction(visitedLocation, attraction)) {

						user.addUserReward(new UserReward(visitedLocation, attraction, this.getRewardPoints(attraction, user)));

					}
				}
			}
		}*/
		Iterable<VisitedLocation> userLocations = new ArrayList<>(user.getVisitedLocations());
		List<Attraction>          attractions   = this.gpsUtilService.getAllAttraction();

		for (VisitedLocation visitedLocation : userLocations) {

			for (Attraction attraction : attractions) {

				if (user.getUserRewards()
						.stream()
						.noneMatch(r -> r.attraction.attractionName.equals(attraction.attractionName))) {

					if(rewardsService.nearAttraction(visitedLocation, attraction)) {

						user.addUserReward(new UserReward(visitedLocation, attraction,
								this.rewardsService.getRewardPoints(attraction, user)
						));
					}
				}
			}
		}

		//});


	}

	public CompletableFuture<Map<Double, Attraction>> distanceAttractions (UUID userId){
		CompletableFuture<Map<Double, Attraction>> future
				= CompletableFuture.supplyAsync(() -> {// recupere la position de l'utilisateur
			VisitedLocation visitedLocation = gpsUtilService.getUserLocation(userId);
			// recupere toutes les attractions
			List<Attraction> attractions = gpsUtilService.getAllAttraction();
			// pour chaque attraction
			// calculer distance entre ustilisateur et attraction

// stocker le resultat
			Map<Double, Attraction> distances = new TreeMap<>(new Comparator<Double>() {
				@Override
				public int compare(Double o1, Double o2) {
					return o1.compareTo(o2);
				}
			});

			for(Attraction attr: attractions) {
				double distance = gpsUtilService.getDistance(visitedLocation.location, attr);
				distances.put(distance, attr);
			}


			// renvoyer les 5 attractions les plus proches
			return distances;
		});
		return future;

	}
	public int getRewardPoints(Attraction attraction, User user) {

		return rewardsService.getRewardPoints(attraction, user);
	}

	public Map<String, Location> getAllCurrentLocation(){
		// récuperer dans une map la liste des users
		Map<String, Location> userLastLocation = new HashMap<String, Location>();
		List<User> users = getAllUsers();

		// récupere le dernier élément de la liste dans la boucle

		for(User user: users) {
			VisitedLocation  lastLocation = user.getVisitedLocations().get(user.getVisitedLocations().size()-1);
			userLastLocation.put(user.getUserId().toString(), lastLocation.location);
		}
		return userLastLocation;
	}



	public List<NearAttractionDto> getNearByAttraction(String userName) throws ExecutionException, InterruptedException {
		User user = getUser(userName);



		Map<Double, Attraction> distanceAttraction = distanceAttractions(user.getUserId()).get();
		// pour chaque entree de la map
		// creer un NearByDTO avec les infos de l'attraction et de la distance
		// ajouter a la liste
		// cree une liste vide NearByLocationDTO
		List<NearAttractionDto> nearByAttractions = new ArrayList<>();
		// parcourir ta map
		distanceAttraction.forEach((distance, attraction) -> {
			NearAttractionDto nearAttractionDto = null;

				nearAttractionDto = new NearAttractionDto(
						attraction.attractionName,
						new Location(attraction.latitude, attraction.longitude),
						user.getLastVisitedLocation().location,
						distance,
						this.getRewardPoints(attraction, user));


			nearByAttractions.add(nearAttractionDto);


		});

		return nearByAttractions;
	}

	// parametre = userNarme, UserPreferences
	// chercher le user selon userName dans le repository
	// on affecte userPreferences dans user
	public void preferenceUser(String userName, UserPreferences userPreferences){
		User user = getUser(userName);
		user.setUserPreferences(userPreferences);
	}

	public ExecutorService getExecutorService() {
		return executorService;
	}
}