<h1>Tourguide</h1>

Tour Guide est une application destinée à tous ceux qui souhaitent partir en voyages tout en obtenant des informations sur les attractions touristiques.

<h1>Pré-requis</h1>
Install Java 17

Install gradle 7.5.1


<h1>Clone</h1>
git clone https://gitlab.com/SabHicham/tripmaster

<h1>Test</h1>
./gradlew test

<h1>build</h1>
./gradlew build


<h1>lancer le projet</h1>
java -jar build/libs/TourGuide.jar

<h1>Rapport Jacoco</h1>
$ ./gradlew jacocoTestReport

$ ./gradlew jacocoTestCoverageVerification
